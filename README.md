# README #
 
# MarvelCharacterFinderControll.class #
Essa classe representa a primeira API proposta no desafio, que acessa a API Marvel em dois momentos: um para listar todos os personagens e outro que busca somente um personagem (por id). Esse acesso está sendo feito via Jersey.

# MarvelDataHandler.class #
Representa a segunda API que deve manipular e armazenar os dados trazidos pela API anterior (MarvelCharacterFinderControll.class).

# MarvelDataHandlerClient.class #
Essa classe contém as URLs válidas para acessar os métodos da classe MarvelDataHandler.class funcionando como uma espécie de cliente.

Além disso, foi usada uma API que possui classes que representam Json dos personagens Marvel para facilitar a conversão de Json em objetos que possam ser mantidos em memória.

URLs úteis (considerando localhost):

http://localhost:8080/DesafioCIT/rest/marvel/characters - Lista todos os personagens

http://localhost:8080/DesafioCIT/rest/marvel/characters/{id} - Lista apenas o personagem de id especificado

http://localhost:8080/DesafioCIT/rest/client/find/characters/ - Lista os personagens carregados na primeira API.

http://localhost:8080/DesafioCIT/rest/client/update/character/{id} - Busca novamente o personagem na API Marvel e substitui o antigo pelo novo. Caso o personagem não exista na lista, será adicionado.

http://localhost:8080/DesafioCIT/rest/client/remove/character/{id} - Remove o personagem de id especificado.