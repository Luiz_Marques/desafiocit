package com.cit.desafio.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cit.desafio.model.Character;
import com.cit.desafio.rest.MarvelDataHandler;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/applicationContext.xml" })
public class MarvelDataHandlerTest {

	@Resource
	private MarvelDataHandler handler;
	
	@Test
	public void emptyCharactersListMessageShouldBeReturned() {
		// Given
		MarvelDataHandler.characters.clear();

		// When
		final String message = this.handler.findCharacters();

		// Then
		assertEquals(MarvelDataHandler.MESSAGE_EMPTY_CHARACTERS_LIST, message);
	}

	@Test
	public void characterShouldBeRemoved() {
		// Given
		mockCharactersList();
		final Character characterToBeRemoved = new Character("100200", "Iron Man", "Tony Stark");
		final int sizeBeforeRemove = MarvelDataHandler.characters.size();

		// When
		this.handler.removeCharacter(characterToBeRemoved.getId());

		// Then
		assertFalse(MarvelDataHandler.characters.contains(characterToBeRemoved));
		assertEquals(sizeBeforeRemove - 1, MarvelDataHandler.characters.size());
	}

	@Test
	public void characterShouldBeNotFound() {
		// Given
		mockCharactersList();
		final Character characterToBeRemoved = new Character("100500", "Super Man - I'm not a marvel character :P", "Clark Kent");
		final int sizeBeforeRemove = MarvelDataHandler.characters.size();

		// When
		String message = this.handler.removeCharacter(characterToBeRemoved.getId());

		// Then
		assertEquals(MarvelDataHandler.MESSAGE_CHARACTER_NOT_FOUND, message);
		assertEquals(sizeBeforeRemove, MarvelDataHandler.characters.size());
	}

	@Test
	public void characterShouldBeUpdated() {
		// Given
		final String characterToBeUpdatedJson = "{\"code\":200,\"status\":\"Ok\",\"copyright\":\"� 2016 MARVEL\",\"attributionText\":\"Data provided by Marvel. � 2016 MARVEL\",\"attributionHTML\":\"<a href=\\\"http://marvel.com\\\">Data provided by Marvel. � 2016 MARVEL</a>\",\"etag\":\"c786787b9e76610f19d210d4fa4e4c8378e6ec1a\",\"data\":{\"offset\":0,\"limit\":20,\"total\":1,\"count\":1,\"results\":[{\"id\":1010870,\"name\":\"Ajaxis\",\"description\":\"\",\"modified\":\"1969-12-31T19:00:00-0500\",\"thumbnail\":{\"path\":\"http://i.annihil.us/u/prod/marvel/i/mg/b/70/4c0035adc7d3a\",\"extension\":\"jpg\"},\"resourceURI\":\"http://gateway.marvel.com/v1/public/characters/1010870\",\"comics\":{\"available\":0,\"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010870/comics\",\"items\":[],\"returned\":0},\"series\":{\"available\":0,\"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010870/series\",\"items\":[],\"returned\":0},\"stories\":{\"available\":0,\"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010870/stories\",\"items\":[],\"returned\":0},\"events\":{\"available\":0,\"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010870/events\",\"items\":[],\"returned\":0},\"urls\":[{\"type\":\"detail\",\"url\":\"http://marvel.com/characters/113/ajaxis?utm_campaign=apiRef&utm_source=d71201ee86bac11d72f69ec4b60bb845\"},{\"type\":\"wiki\",\"url\":\"http://marvel.com/universe/Ajaxis?utm_campaign=apiRef&utm_source=d71201ee86bac11d72f69ec4b60bb845\"},{\"type\":\"comiclink\",\"url\":\"http://marvel.com/comics/characters/1010870/ajaxis?utm_campaign=apiRef&utm_source=d71201ee86bac11d72f69ec4b60bb845\"}]}]}}";
		final String characterId = "1010870";
		
		// When
		this.handler.updateCharacter(characterToBeUpdatedJson);
		
		// Then
		final Character updatedCharacter = MarvelDataHandler.characters.stream()
				.filter(character -> characterId.equals(character.getId())).findFirst().orElse(null);
		
		assertEquals("Ajaxis", updatedCharacter.getName());
	}
	
	private void mockCharactersList() {
		final Set<Character> mockedCharacters = new HashSet<>();
		mockedCharacters.add(new Character("100200", "Iron Man", "Tony Stark"));
		mockedCharacters.add(new Character("100300", "Spider Man", "Peter Park"));
		mockedCharacters.add(new Character("1010870", "I have a valid marvel Id!", "I have a valid marvel Id!"));

		MarvelDataHandler.characters.clear();
		MarvelDataHandler.characters.addAll(mockedCharacters);
	}
}
