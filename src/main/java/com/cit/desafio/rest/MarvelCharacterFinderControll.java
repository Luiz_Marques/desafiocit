package com.cit.desafio.rest;

import static com.cit.desafio.constants.MarvelConstants.MARVEL_DATA_HANDLER_ADD_CHARACTERS_URL;
import static com.cit.desafio.constants.MarvelConstants.MARVEL_GEL_CHARACTER_END_POINT_URL;
import static com.cit.desafio.constants.MarvelConstants.MARVEL_GET_ALL_CHARACTER_URL;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cit.desafio.service.TransactionService;
import com.sun.jersey.api.client.WebResource;

import us.monoid.json.JSONException;

/**
 * First API of the CI&T challenge: Class to access Marvel API to bring data.
 */
@Component
@Path("/marvel")
public class MarvelCharacterFinderControll {

	@Autowired
	private TransactionService transactionService;

	@GET
	@Path("/characters")
	public Response getCharacters() throws JSONException {
		// calling Marvel API first time - Getting all characters
		final String result = this.transactionService.callMarvelAPI(MARVEL_GET_ALL_CHARACTER_URL);

		// calling a class that manipulate the result provided by Marvel API
		final WebResource webResource = this.transactionService.createWebResource(MARVEL_DATA_HANDLER_ADD_CHARACTERS_URL);
		final String response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(String.class, result);
		return Response.status(200).entity(response).entity(result).build();
	}
	
	@GET
	@Path("/characters/{id}")
	public Response getCharacter(@PathParam("id") String id) {
		// calling Marvel API second time - Getting a character by id
		final String urlWithParam = MARVEL_GEL_CHARACTER_END_POINT_URL + id + "?ts=%d&apikey=%s&hash=%s";
		final String result = this.transactionService.callMarvelAPI(urlWithParam);

		// calling a class that manipulate the result provided by Marvel API
		final WebResource webResource = this.transactionService.createWebResource(MARVEL_DATA_HANDLER_ADD_CHARACTERS_URL);
		final String response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(String.class, result);
		return Response.status(200).entity(response).entity(result).build();
	}
}
