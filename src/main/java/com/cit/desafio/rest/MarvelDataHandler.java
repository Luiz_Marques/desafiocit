package com.cit.desafio.rest;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cit.desafio.model.Character;
import com.cit.desafio.service.MarvelCharacterService;
import com.google.gson.Gson;

/**
 * Second API of the CI&T challenge: Class to manipulate the data bring from
 * first API.
 */
@Component
@Path("/handle")
public class MarvelDataHandler {

	// all methods are returning messages to make easier the
	// tests/implementation
	public static final String MESSAGE_EMPTY_CHARACTERS_LIST = "The list is empty! To solve that, execute some GET rest method present in MarvelCharacterFinder.class =)";
	public static final String MESSAGE_CHARACTERS_ADDED = "All Marvel characters found were added into the list!";
	public static final String MESSAGE_CHARACTER_REMOVED = "The character %s was removed!";
	public static final String MESSAGE_CHARACTER_NOT_FOUND = "Character not found!";
	public static final String MESSAGE_CHARACTERS_UPDATED = "Character updated!";

	@Autowired
	private MarvelCharacterService marvelCharacterService;
	private Gson gson;

	// keep in memory the not repeated characters loaded
	public static Set<Character> characters;

	@PostConstruct
	public void init() {
		MarvelDataHandler.characters = new HashSet<Character>();
		this.gson = new Gson();
	}

	@GET
	@Path("/find/characters")
	public String findCharacters() {
		// the find in this case, will be return the static characters list
		return MarvelDataHandler.characters.isEmpty() ? MESSAGE_EMPTY_CHARACTERS_LIST
				: this.gson.toJson(MarvelDataHandler.characters);
	}

	@POST
	@Path("/add/characters")
	@Consumes(MediaType.APPLICATION_JSON)
	public String addNewCharacters(String json) throws Exception {
		// adding last found characters into list
		MarvelDataHandler.characters.addAll(this.marvelCharacterService.extractCharacters(json));
		return MESSAGE_CHARACTERS_ADDED;
	}

	@DELETE
	@Path("/remove/character/{id}")
	public String removeCharacter(@PathParam("id") String id) {
		final Character toBeRemoved = MarvelDataHandler.characters.stream()
				.filter(character -> id.equals(character.getId())).findFirst().orElse(null);

		if (null == toBeRemoved) {
			return MESSAGE_CHARACTER_NOT_FOUND;
		}

		MarvelDataHandler.characters.removeIf(character -> id.equals(character.getId()));
		return String.format(MESSAGE_CHARACTER_REMOVED, toBeRemoved.getName());
	}

	@PUT
	@Path("/update/character")
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateCharacter(String json) {
		// replacing old object to updated object
		final Character extractCharacter = this.marvelCharacterService.extractCharacter(json);
		MarvelDataHandler.characters.removeIf(character -> character.getId().equals(extractCharacter.getId()));
		MarvelDataHandler.characters.add(extractCharacter);
		
		return MESSAGE_CHARACTERS_UPDATED;
	}
}
