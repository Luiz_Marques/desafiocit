package com.cit.desafio.rest;

import static com.cit.desafio.constants.MarvelConstants.MARVEL_DATA_HANDLER_FIND_CHARACTER_URL;
import static com.cit.desafio.constants.MarvelConstants.MARVEL_DATA_HANDLER_REMOVE_CHARACTER_URL;
import static com.cit.desafio.constants.MarvelConstants.MARVEL_DATA_HANDLER_UPDATE_CHARACTER_URL;
import static com.cit.desafio.constants.MarvelConstants.MARVEL_GEL_CHARACTER_END_POINT_URL;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cit.desafio.service.TransactionService;
import com.sun.jersey.api.client.WebResource;

/**
 * This class was created just to test the methods of MarvelDataHandler.class. 
 * This is a kind of "Client" to access MarvelDataHandler.class.
 */
@Component
@Path("/client")
public class MarvelDataHandlerClient {
	
	@Autowired
	private TransactionService transactionService;

	@GET
	@Path("/find/characters")
	public Response findCharacters() {
		final WebResource webResource = this.transactionService.createWebResource(MARVEL_DATA_HANDLER_FIND_CHARACTER_URL);
		final String response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get(String.class);
		return Response.status(200).entity(response).build();
	}
	
	@GET
	@Path("/update/character/{id}")
	public Response updateCharacter(@PathParam("id") String id) {
		// bring the updated character from api.
		final String urlWithParam = MARVEL_GEL_CHARACTER_END_POINT_URL + id + "?ts=%d&apikey=%s&hash=%s";
		final String updatedCharacterJson = this.transactionService.callMarvelAPI(urlWithParam);
		
		final WebResource webResource = this.transactionService.createWebResource(MARVEL_DATA_HANDLER_UPDATE_CHARACTER_URL);
		final String response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).put(String.class, updatedCharacterJson);
		return Response.status(200).entity(response).build();
	}
	
	@GET
	@Path("/remove/character/{id}")
	public Response deleteCharacter(@PathParam("id") String id) {
		final WebResource webResource = this.transactionService.createWebResource(MARVEL_DATA_HANDLER_REMOVE_CHARACTER_URL + "/" + id);
		final String response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).delete(String.class, id);
		return Response.status(200).entity(response).build();
	}
}
