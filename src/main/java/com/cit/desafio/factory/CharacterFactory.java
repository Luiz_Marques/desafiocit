package com.cit.desafio.factory;

import java.util.ArrayList;
import java.util.List;

import com.cit.desafio.model.Character;
import com.karumi.marvelapiclient.model.CharacterDto;

public class CharacterFactory {

	public static List<Character> getCharacters(final List<CharacterDto> characterDtos) {
		List<Character> characters = new ArrayList<>();
		characterDtos.forEach(characterDto -> characters.add(getCharacter(characterDto)));
		return characters;
	}

	public static Character getCharacter(final CharacterDto characterDto) {
		return new Character(characterDto);
	}
}
