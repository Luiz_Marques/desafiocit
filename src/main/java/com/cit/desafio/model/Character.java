package com.cit.desafio.model;

import com.karumi.marvelapiclient.model.CharacterDto;

/**
 * This is a simple class to represent a Character inside data handler. The main
 * motivation to create this class, was the fact that CharacterDto.class does
 * not have equals() and is not possible override it.
 */
public class Character {

	private String id;
	private String name;
	private String description;

	public Character(String id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Character(final CharacterDto characterDto) {
		this.id = characterDto.getId();
		this.name = characterDto.getName();
		this.description = characterDto.getDescription();
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Character other = (Character) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}