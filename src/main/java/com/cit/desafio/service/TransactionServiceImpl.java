package com.cit.desafio.service;

import static com.cit.desafio.constants.MarvelConstants.PRIVATE_KEY;
import static com.cit.desafio.constants.MarvelConstants.PUBLIC_KEY;

import org.apache.commons.codec.digest.DigestUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class TransactionServiceImpl implements TransactionService {

	@Override
	public String callMarvelAPI(final String path) {
		// call Marvel API by Jersey
		WebResource webResource = createWebResource(buildUrl(path));
		ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
		return response.getEntity(String.class);
	}

	@Override
	public WebResource createWebResource(String path) {
		Client client = Client.create();
		return client.resource(path);
	}

	private String buildUrl(final String rootPath) {
		long timeStamp = System.currentTimeMillis();
		String hash = DigestUtils.md5Hex(timeStamp + PRIVATE_KEY + PUBLIC_KEY);
		return String.format(rootPath, timeStamp, PUBLIC_KEY, hash);
	}
}