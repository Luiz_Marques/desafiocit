package com.cit.desafio.service;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import com.cit.desafio.factory.CharacterFactory;
import com.cit.desafio.model.Character;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.marvelapiclient.model.CharacterDto;
import com.karumi.marvelapiclient.model.CharactersDto;
import com.karumi.marvelapiclient.model.MarvelResponse;

public class MarvelCharacterServiceImpl implements MarvelCharacterService {

	@Override
	public List<Character> extractCharacters(final String json) {
		if (isBlank(json)) {
			return Collections.emptyList();
		}

		// mapping Json to MarvelResponse.class (provided by Marvel Client API)
		Type type = new TypeToken<MarvelResponse<CharactersDto>>() {
		}.getType();

		MarvelResponse<CharactersDto> result = new Gson().fromJson(json, type);
		List<CharacterDto> charactersDto = result.getResponse().getCharacters();

		return CharacterFactory.getCharacters(charactersDto);
	}

	@Override
	public Character extractCharacter(final String json) {
		// all validation rules... 
		// ... 
		
		return this.extractCharacters(json).get(0);
	}
}
