package com.cit.desafio.service;

import com.sun.jersey.api.client.WebResource;

public interface TransactionService {

	String callMarvelAPI(String path);
	
	WebResource createWebResource(String path);
}