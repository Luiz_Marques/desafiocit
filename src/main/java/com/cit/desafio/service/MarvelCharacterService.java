package com.cit.desafio.service;

import java.util.List;

import com.cit.desafio.model.Character;

public interface MarvelCharacterService {

	List<Character> extractCharacters(String json);

	Character extractCharacter(String json);
}
