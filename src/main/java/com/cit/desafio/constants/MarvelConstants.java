package com.cit.desafio.constants;

public class MarvelConstants {

	// TODO: move this to a .properties file

	// Marvel API keys
	public static final String PUBLIC_KEY = "d71201ee86bac11d72f69ec4b60bb845";
	public static final String PRIVATE_KEY = "01e3e7e3c1221ddde325c41197a818a171989157";

	// Marvel API URLs
	public static final String MARVEL_GET_ALL_CHARACTER_URL = "http://gateway.marvel.com/v1/public/characters?ts=%d&apikey=%s&hash=%s";
	public static final String MARVEL_GEL_CHARACTER_END_POINT_URL = "http://gateway.marvel.com:80/v1/public/characters/";
	
	// Local data handler URLs
	public static final String MARVEL_DATA_HANDLER_ADD_CHARACTERS_URL = "http://localhost:8080/DesafioCIT/rest/handle/add/characters";
	public static final String MARVEL_DATA_HANDLER_REMOVE_CHARACTER_URL = "http://localhost:8080/DesafioCIT/rest/handle/remove/character";
	public static final String MARVEL_DATA_HANDLER_UPDATE_CHARACTER_URL = "http://localhost:8080/DesafioCIT/rest/handle/update/character";
	public static final String MARVEL_DATA_HANDLER_FIND_CHARACTER_URL = "http://localhost:8080/DesafioCIT/rest/handle/find/characters";
}
